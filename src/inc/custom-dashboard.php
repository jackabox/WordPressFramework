<?php
/**
 * Our custom dashboard page
 */

/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );
?>

<div class="wrap about-wrap">
    <h1>Welcome to Your Dashboard</h1>
</div>

<?php include( ABSPATH . 'wp-admin/admin-footer.php' );