<?php // ==== FUNCTIONS ==== //

# Load in Adtrak's default functions
if ( is_readable( trailingslashit( get_stylesheet_directory() ) . 'functions-adtrak.php' ) )
  require_once( trailingslashit( get_stylesheet_directory() ) . 'functions-adtrak.php' );

# Load in configuration defaults here
require_once( trailingslashit( get_stylesheet_directory() ) . 'functions-config.php' );

# WooCommerce functions
require_once( trailingslashit( get_stylesheet_directory() ) . 'functions-woocommerce.php' );

// An example of how to manage loading front-end assets (scripts, styles, and fonts)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/assets.php' );

// Required to demonstrate WP AJAX Page Loader (as WordPress doesn't ship with simple post navigation functions)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/navigation.php' );



// Only the bare minimum to get the theme up and running
function adtrak_setup() {
  load_theme_textdomain( 'adtrak', trailingslashit( get_template_directory() ) . 'languages' );
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  global $content_width;
  if ( !isset( $content_width ) || !is_int( $content_width ) )
    $content_width = (int) 1200;

  register_nav_menu( 'header', __( 'Header menu', 'adtrak' ) );
  register_nav_menu( 'footer', __( 'Footer menu', 'adtrak' ) );

}
add_action( 'after_setup_theme', 'adtrak_setup', 11 );

// Sidebar declaration
function adtrak_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main sidebar', 'adtrak' ),
    'id'            => 'sidebar-main',
    'description'   => __( 'Appears to the right side of most posts and pages.', 'adtrak' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ) );
}
add_action( 'widgets_init', 'adtrak_widgets_init' );

# Emjoi
if ( ADTRAK_EMJOI_DISABLE ) {
    function disable_emojis() {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );    
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    }
    add_action( 'init', 'disable_emojis' );

    function disable_emojis_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) {
            return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
            return array();
        }
    }
}

