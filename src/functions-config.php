<?php // ==== CONFIGURATION (DEFAULT) ==== //

// Specify default configuration values here; these may be overwritten by previously defined values in `functions-config.php`

// Switch for Headroom.js; true/false
defined( 'ADTRAK_SCRIPTS_HEADROOM' )       || define( 'ADTRAK_SCRIPTS_HEADROOM', true );

// Switch to enable WP emjoi injection true/false
defined( 'ADTRAK_EMJOI_DISABLE' )       || define( 'ADTRAK_EMJOI_DISABLE', true );