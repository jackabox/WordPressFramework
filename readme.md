# About

This utilises the built theme, which is stripped down. [https://github.com/synapticism/wordpress-gulp-bower-sass](). I then included bower and neat, stripped out their sass and rebuilt it using one I've built over the years. See their documentation for how to roll with it.

If it doesn't work with Yeoman and symlinks, change the gulpfile to compile build to the wp-content/themes directory.